setup: env-prepare sqlite-prepare install key db-prepare run-dev-server

install:
	composer install
	npm ci

local:
	heroku local -f Procfile.dev

db-prepare:
	php artisan migrate --seed

env-prepare:
	cp -n .env.example .env || true

sqlite-prepare:
	touch database/database.sqlite

key:
	php artisan key:generate

run-dev-server:
	php artisan serve

ide-helper:
	php artisan ide-helper:eloquent
	php artisan ide-helper:gen
	php artisan ide-helper:meta
	php artisan ide-helper:mod -n

.PHONY: test
