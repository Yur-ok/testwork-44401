<?php


namespace App\Http\Controllers;


use App\Models\Article;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function main()
    {
        return view('page.main');
    }

    public function about()
    {
        return view('page.about');
    }

    public function search(Request $request)
    {
        $key = trim($request->get('q'));
        $articles = Article::query()
            ->where('title', 'like', "%{$key}%")
            ->orWhere('body', 'like', "%{$key}%")
            ->orderBy('created_at', 'desc')
            ->get();

        return view('page.search', compact('articles', 'key'));
    }
}
