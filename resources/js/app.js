require('./bootstrap');

// Code for removing button
// It's convert any in form according to data-attribute
const ujs = require('@rails/ujs');
ujs.start();

/* Code for changing active link on clicking */
$(function () {
    const $links = $('.navbar a');
    $.each($links, function (index, link) {
        if (link.href === document.URL) {
            $(this).addClass('active');
        }
    });
});
