@if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="form-group">
    {{ Form::label('title', 'Название', ['size' => 200]) }}
    {{ Form::text('title') }}<br>
</div>
<div class="form-group">
    {{ Form::label('body', 'Содержание') }}
    {{ Form::textarea('body') }}<br>
</div>
