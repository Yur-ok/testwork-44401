@extends('layouts.app')

@section('content')
    <h1>{{$article->title}}</h1>
    <div class="text-break">{{$article->body}}</div>
    <h5 class="mt-4 fa fa-edit fa-lg"><a href="{{route('articles.edit', $article)}}"> Редактировать статью</a></h5>
@endsection
