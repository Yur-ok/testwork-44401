@extends('layouts.app')

@section('content')
    <h1 class="display-4 text-center">Список статей</h1>
    <h3 class="my-3">
        <a href="{{route('articles.create')}}"
           class="fa fa-newspaper-o btn btn-success" role="button"> Create new</a>
    </h3>
    <ul class="list-unstyled">
        <li class="media">
            <div class="media-body">
                @foreach ($articles as $article)
                    <h2 class="mt-0 mb-1 font-weight-bold">
                        <a href="{{ route('articles.show', $article) }}">{{$article->title}}</a>
                    </h2>
                    <div class="mb-4 text-break">{{Str::limit($article->body, 300)}}</div>
                    <h6 class="text-right">
                        <a href="{{ route('articles.destroy', $article) }}"
                           data-confirm="Вы уверены?"
                           data-method="delete" class="fa fa-remove btn btn-outline-danger text-danger"
                           rel="nofollow">
                            Delete
                        </a>
                    </h6>

                @endforeach
            </div>
        </li>
    </ul>

    <ul class="pagination justify-content-center mb-4">
        {{ $articles->links("pagination::bootstrap-4") }}
    </ul>
@endsection
