@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::model($article, ['url' => route('articles.store')]) }}
        @include('article.form')
        {{ Form::submit('Создать') }}
        {{ Form::close() }}
    </div>
@endsection
