@extends('layouts.app')

@section('content')
    <h2>You request <span class="font-weight-bold">{{ $key }}</span> found in:</h2>

    <div class="media-body">
        @foreach ($articles as $article)
            <h2 class="mt-0 mb-1 font-weight-bold">
                <a href="{{ route('articles.show', $article) }}">{{$article->title}}</a>
            </h2>
            <div class="mb-4 text-break">{{Str::limit($article->body, 300)}}</div>
            <h6 class="text-right">
                <a href="{{ route('articles.destroy', $article) }}"
                   data-confirm="Вы уверены?"
                   data-method="delete" class="fa fa-remove btn btn-outline-danger text-danger"
                   rel="nofollow">
                    Delete
                </a>
            </h6>

        @endforeach
    </div>
@endsection
