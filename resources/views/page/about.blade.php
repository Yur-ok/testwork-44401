@extends('layouts.app')

@section('content')
    <div class="text-center d-flex flex-column justify-content-center">
        <h3 class="mb-4">Данный проект ялвяет собой пример классического CRUD приложения.</h3>
        <p class="h4">Созданный специально для компании
            <a href="https://talentonline.eu/" target="_blank">Talent Online</a>
            <span class="fa fa-external-link text-primary"></span>
        </p>
        <p class="h5 mt-5">
            <span class="font-weight-bold text-secondary">Автор:</span> <span>Бачевский Юрий</span>
        </p>
        <a href="https://github.com/yur-ok" target="_blank">
            <span class="fa fa-github-alt fa-4x text-primary"></span>
        </a>
    </div>
@endsection
