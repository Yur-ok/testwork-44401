@extends('layouts.app')

@section('content')
    <h2 class="text-center mb-5">Главная станица проекта</h2>
    <p class="h4 text-break col-10">
        Данный учебный проект демонстрирует пример классичесокго CRUD-приложения. <br>
        В котором реализован базовый функционал для работы с сущностью проекта. <br>
        В данном примере сущность представленна в виде статьи.
    </p>

    <p class="h4 col-10">
        На примере статьи продемонстрированы базовые операции:
        <ul class="h4 list-group">
            <li class="list-group-item"><span class="font-weight-bolder text-primary">C</span>reate</li>
            <li class="list-group-item"><span class="font-weight-bolder text-primary">R</span>ead</li>
            <li class="list-group-item"><span class="font-weight-bolder text-primary">U</span>pdate</li>
            <li class="list-group-item"><span class="font-weight-bolder text-primary">D</span>elete</li>
        </ul>
    </p>
@endsection
